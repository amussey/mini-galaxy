SHELL := /bin/bash
PYTHON_EXE := python3.6
PYTHON_ENV := ./env
PYTHON_BIN := $(PYTHON_ENV)/bin
PYTHON_REQUIREMENTS := ./requirements.txt
ENV := dev


DEFAULT: help


#================================================================================
# Environment
#================================================================================

.PHONY: requirements.txt
requirements.txt:  ## Write the requirements.txt file.
	$(PYTHON_BIN)/pip freeze > $(PYTHON_REQUIREMENTS)

virtualenv:  ## Build the python virtual environment.
	@echo -e "Building/verifying virtualenv at $(PYTHON_ENV) based on $(PYTHON_REQUIREMENTS)\n"
	@command -v pip >/dev/null 2>&1 || { echo >&2 "I require pip but it's not installed.  Aborting."; exit 1; }
	@if [ ! -f "$(PYTHON_ENV)/bin/activate" ] ; then \
		virtualenv -p $(PYTHON_EXE) $(PYTHON_ENV) ; \
	fi
	$(PYTHON_BIN)/pip install -q -r $(PYTHON_REQUIREMENTS)

.PHONY: env
env:
	for f in secrets/*.json; do \
		e=$${f%.*} ; \
		e=$${e##*/} ; \
		echo "Processing $$e file.." ; \
		$(PYTHON_BIN)/$(PYTHON_EXE) tools/jsonenv.py < $$f > .env_$$e ; \
	done


.PHONY: run
run:  ## Run the flask app.
run: virtualenv env
	(sleep 1; open "http://127.0.0.1:8000/") &
	source .env_$(ENV) && $(PYTHON_BIN)/gunicorn mini_galaxy:app --reload


#================================================================================
# Deployment
#================================================================================

secrets-deploy:  ## Deploy the secrets for the dev environment.
	aws s3 sync secrets s3://ansible-mini-galaxy/secrets


.PHONY: deploy deploy-dev
deploy: deploy-dev
deploy-dev:  ## Deploy the dev Lambda app.
deploy-dev: virtualenv secrets-deploy
	source $(PYTHON_BIN)/activate ; \
	zappa deploy dev


.PHONY: update update-dev
update: update-dev
update-dev:  ## Update the code for the dev Lambda app.
update-dev: virtualenv secrets-deploy
	source $(PYTHON_BIN)/activate ; \
	zappa update dev


.PHONY: deploy-prod
deploy-prod:  ## Deploy the production Lambda app.
deploy-prod: virtualenv secrets-deploy
	source $(PYTHON_BIN)/activate ; \
	zappa deploy prod


.PHONY: update-prod
update-prod:  ## Update the code for the production Lambda app.
update-prod: virtualenv secrets-deploy
	source $(PYTHON_BIN)/activate ; \
	zappa update prod


#================================================================================
# Cleanliness
#================================================================================

.PHONY: lint
lint:  ## Lint the Python app.
lint: virtualenv
	$(PYTHON_BIN)/flake8 mini_galaxy/*.py mini_galaxy/**/*.py

flake8: lint


#================================================================================
# Extras
#================================================================================

.PHONY: help
help:  ## This help dialog.
	@echo -e  "You can run the following commands from this$(MAKEFILE_LIST):\n"
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//'`) ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$'#' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf "  %-27s %s\n" $$help_command $$help_info ; \
	done
