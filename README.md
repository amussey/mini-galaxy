# Mini Galaxy


This is designed to a small, portable version of the [Ansible Galaxy](https://github.com/ansible/galaxy).

It is designed to be deployed with Zappa to AWS Lambda, be backed by S3, and leverage the API Gateway.

