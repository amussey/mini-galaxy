from flask import Flask, redirect, Response
import os
from nodb import NoDB


app = Flask(__name__)


db = NoDB()
db.bucket = 'ansible-mini-galaxy'
db.index = 'id'
db.prefix = '.nodb/{}/'.format(os.environ.get('ENVIRONMENT', 'dev'))


@app.route('/')
def base():
    return 'Mini Galaxy Service'


@app.route('/robots.txt')
def robots_txt():
    robots_txt_contents = '\n'.join([
        'User-agent: *',
        'Disallow: /',
    ])
    return Response(robots_txt_contents, mimetype='text/plain')


if __name__ == '__main__':
    app.run(debug=True)
